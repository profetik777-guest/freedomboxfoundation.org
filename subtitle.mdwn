# Help out without leaving your web browser

We generate a decent amount of video while telling people about the FreedomBox project, almost all of which is in English. This is great for people who speak English and like watching video but poses some difficulty for the rest of the world. If you happen to enjoy our videos, you can help other people access them by taking a minute to subtitle the video, turning it instantly into text that can be widely accesses and translated. 

If you have a modern web browser installed, you already have everything you need to help out. Simply visit our [media page](http://freedomboxfndn.mirocommunity.org) and look at the subtitle menu directly below each video. If any of them say "Subtitle Me!" just click there and you are ready to start. You will need an account with [Universal Subtitles](http://universalsubtitles.org/), a project run by the [Participatory Culture Foundation](http://pculture.org/).

## Current videos we need help subtitling

Jame Vasile's "[Elevate 2011](http://freedomboxfndn.mirocommunity.org/video/9/elevate-2011)" talk from the 2011 Elevate conference in Graz, October 24, 2011

Bdale Garbee's "[FreedomBox progress report](http://freedomboxfndn.mirocommunity.org/video/7/freedombox-progress-report)" from the 2011 DebConf, August 12, 2011

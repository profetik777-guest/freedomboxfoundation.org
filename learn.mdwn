# Learn About the FreedomBox!

Eben Moglen explained the FreedomBox in a short interview with the
[CBS Evening News](http://freedomboxfoundation.org/cbs).  We also have
a [one page flyer](/doc/flyer.pdf) that explains the box quickly and
easily to lay people.  Print and distribute it at your next
conference!

## What is FreedomBox?

* Email and telecommunications that protects privacy and resists eavesdropping

* A publishing platform that resists oppression and censorship.

* An organizing tool for democratic activists in hostile regimes.

* An emergency communication network in times of crisis.

FreedomBox will put in people's own hands and under their own control 
encrypted voice and text communication, anonymous publishing, social 
networking, media sharing, and (micro)blogging.

Much of the software already exists: onion routing, encryption, virtual 
private networks, etc. There are tiny, low-watt computers known as 
"[plug servers](https://secure.wikimedia.org/wikipedia/en/wiki/Plug_computer)" 
to run this software. The hard parts is integrating that technology,
distributing it, and making it easy to use without expertise. The harder 
part is to decentralize it so users have no need to rely on and trust 
centralized infrastructure.

That's what FreedomBox is: we integrate privacy protection on a cheap 
plug server so everybody can have privacy. Data stays in your home and 
can't be mined by governments, billionaires, thugs or even gossipy 
neighbors.

With FreedomBoxes in their homes, anybody, regardless of technical skill, 
can easily enjoy secure, private, even anonymous communication!

## Why FreedomBox?

FreedomBox integrates privacy protection on a cheap plug server so
everybody can have privacy. Data stays in your home and can't be mined
by governments, billionaires, thugs or even gossipy neighbors. Other
practical examples where FreedomBox is useful:

 * FreedomBoxes are encrypted web proxies. Boxes in uncensored
   countries can bounce signals for users stuck behind censorship
   walls---each one is a tiny crack in the Great Firewall. Chinese
   users could **surf the entire net free from government
   censorship**.

 * The US government famously sought information about internal
   WikiLeaks communications from Twitter and other social websites. By
   moving our communication from centralized monoliths to
   decentralized servers in our homes, we **protect our data from
   government prying**.

 * Many whistleblowers and dissidents need to **anonymously talk to media and the public**. With the FreedomBox, we can use VOIP to encrypt telephone calls and can create anonymous web servers over TOR to publish documents. Anonymous instant messaging or microblogging are also possible.

 * Egyptian Democracy activists had trouble talking to demonstrators in the streets because the Mubarak regime shutdown parts of the internet as well as many cellular networks. **If our internet plug is pulled, the box will use mesh routing to talk to other boxes like it**. If any of them can get a packet across the border, they all can.

 * FreedomBoxes are useful on a daily personal level too. That same proxy technology can **scrub web sites of ads and tracking** technology as you use them, thus protecting your privacy. FreedomBoxes help you **encrypt your email**. They also know who your friends are and can back up your data in encrypted form to their FreedomBoxes. You can get your data back even if you don't know your password. Even absent a crisis, privacy matters.

 * FreedomBox is free software, which means you can freely inspect it,
   audit it, study it and improve it.
 
For a list of specific FreedomBox capabilities, check out our [[Goals]] page.

## Who is FreedomBox?

FreedomBox is a collaborative project of programmers around the world
who believe in Free Software, Free Society.  Many of its members will
come from the [Debian community](http://wiki.debian.org/FreedomBox),
and many will come from other corners of the Free World.  [Bdale
Garbee](http://gag.com/~bdale/), former Debian Project Leader is the
project's Tech Lead.

The FreedomBox Foundation, which supports the FreedomBox Project and
conserves the free software it makes, is led by its President, [Eben
Moglen](http://emoglen.law.columbia.edu/), Professor of Law at
Columbia Law School and founder of the [Software Freedom Law
Center](http://softwarefreedom.org),  [Bdale
Garbee](http://gag.com/~bdale/), former Debian Project Leader is the
project's Tech Lead and President of [Software in the Public
Interest](http://www.spi-inc.org/), and [Yochai
Benkler](http://benkler.org/Bio.html), Berkman Professor of
Entrepreneurial Legal Studies at Harvard, and faculty co-director of
the Berkman Center for Internet and Society.

A fuller description of our team can be found on the [[team]] page.

## FAQ

The Foundation published an [[FAQ|faq]] that starts to answer the more
common questions about next steps and how we relate to the rest of the
community.

## Talks, Press and Appearances

We announce and document talks, press and appearances
[[here|appearances]].

[[!sidebar  content="""

Links:    
[[FAQ]]    
[CBS Evening News](http://freedomboxfoundation.org/cbs)
"""]]

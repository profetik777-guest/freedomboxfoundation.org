# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
#
# Translators:
msgid ""
msgstr ""
"Project-Id-Version: FreedomBox Foundation\n"
"POT-Creation-Date: 2016-02-24 17:43+0000\n"
"PO-Revision-Date: 2011-08-12 16:40+0000\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: French (France) (http://www.transifex.com/projects/p/"
"freedomboxfoundation/language/fr_FR/)\n"
"Language: fr_FR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"FreedomBox IRC meeting\"]]\n"
msgstr ""

#. type: Title #
#, no-wrap
msgid "FreedomBox IRC meeting on Aug 15"
msgstr ""

#. type: Plain text
msgid ""
"FreedomBox Foundation will host a one hour meeting on IRC with executive "
"director James Vasile and tech leader Bdale Garbee on August 15th."
msgstr ""

#. type: Plain text
msgid ""
"You'll be able to ask questions about the advancement of the project, the "
"challenges it is facing and the opportunities that are being tapped."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "Join the channel #freedombox on <a href=\"http://www.oftc.net/oftc/\">OFTC network</a> on Monday the 15th at 9AM UTC-6. If you're not familiar with IRC you can use this <a href=\"http://thegrebs.com/oftc/\">web-based IRC client</a>.\n"
msgstr ""

#. type: Plain text
msgid "Update: read the [[log of the conversation here|20110815-1500UTC-log]]."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!sidebar  content=\"\"\"\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"Links:\n"
"[[Home|https://www.freedomboxfoundation.org/]]  \n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[FAQ]]  \n"
msgstr ""

#. type: Plain text
msgid "[[Donate]]"
msgstr ""

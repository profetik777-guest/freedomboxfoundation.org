Today has been a big day for press coverage of the foundation and a spreading awareness of our project. We started the day in the [NY Times](https://www.nytimes.com/2011/02/16/nyregion/16about.html) with a piece by Jim Dwyer (*Decentralizing the Internet So Big Brother Can’t Find You*). By afternoon there were also pieces in: 

 * The [Wall Street Journal](http://blogs.wsj.com/digits/2011/02/16/freedom-box-needs-a-good-user-interface/) (*Freedom Box Needs A Good User Interface*)
 * [ZDNet](http://www.zdnet.com/blog/networking/freedom-box-freeing-the-internet-one-server-at-a-time/698) (*Freedom Box: Freeing the Internet one Server at a time*)
 * [techPresident](http://techpresident.com/blog-entry/egypt-switches-and-internet-fragility) (*Egypt, Off Switches, and Internet Fragility*)
 * The [New York Observer](http://www.observer.com/2011/tech/debate-will-americans-ever-own-their-own-servers) (*Debate: Will Americans Ever Own Their Own Servers?*)
 * [New Europe](http://www.neurope.eu/articles/Eben-Moglen-A-free-world-needs-free-software/104779.php) (*Eben Moglen: A free world needs free software*)
 * and even the [ABA Journal](http://www.abajournal.com/news/article/law_prof_urges_work_on_freedom_box_to_decentralize_the_internet/) (*Law Prof Urges Work on ‘Freedom Box’ to Decentralize the Internet*), a legal industry publication. 

All these press pieces, and the many related twitter and identi.ca posts, raise some great points about the work we have ahead. We hope to address many of these points over the coming weeks and months as we continue to build the foundation and expand our public activities. All of you who are interested, please stay tuned!

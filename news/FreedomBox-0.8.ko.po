# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2016-02-22 15:21+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Title ##
#, no-wrap
msgid "FreedomBox 0.8 Released"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "*January 20th, 2016*\n"
msgstr ""

#. type: Plain text
msgid ""
"I'm pleased to announce that FreedomBox 0.8 has been released! This release "
"comes 9 weeks after the previous release (0.7)."
msgstr ""

#. type: Plain text
msgid ""
"We did not build new images for this release. However it is available in "
"Debian (unstable) as 2 packages, freedombox-setup 0.8 and plinth 0.8.1-1."
msgstr ""

#. type: Plain text
msgid ""
"If you are using Freedombox 0.7, you can upgrade to 0.8. (If you have "
"automatic upgrades enabled, this should have happened already!) But first, "
"please read the \"Known Issues\" section below."
msgstr ""

#. type: Plain text
msgid "More information on this release is available on the wiki:"
msgstr ""

#. type: Plain text
msgid "<https://wiki.debian.org/FreedomBox/ReleaseNotes>"
msgstr ""

#. type: Plain text
msgid "Major FreedomBox 0.8 Changes:"
msgstr ""

#. type: Bullet: '- '
msgid ""
"Added Quassel, an IRC client that stays connected to IRC networks and can "
"synchronize multiple frontends."
msgstr ""

#. type: Bullet: '- '
msgid "Improved first boot user interface."
msgstr ""

#. type: Bullet: '- '
msgid "Fixed Transmission RPC whitelist issue."
msgstr ""

#. type: Bullet: '- '
msgid ""
"Added translations for Turkish, Chinese, and Russian. Fixed and updated "
"translations in other languages."
msgstr ""

#. type: Bullet: '- '
msgid ""
"Added Monkeysphere, which uses PGP web of trust for SSH host key "
"verification."
msgstr ""

#. type: Bullet: '- '
msgid ""
"Added Let's Encrypt, to obtain certificates for domains, so that browser "
"certificate warnings can be avoided."
msgstr ""

#. type: Bullet: '- '
msgid "Added repro, a SIP server for audio and video calls."
msgstr ""

#. type: Bullet: '- '
msgid ""
"Allow users to set their SSH public keys, so they can login over SSH without "
"a password."
msgstr ""

#. type: Plain text
msgid "Known Issues:"
msgstr ""

#. type: Plain text
msgid ""
"There is an issue in Plinth 0.7.x that can affect those trying to upgrade to "
"0.8.1. The issue happens when the manual upgrade is started (by clicking the "
"\"Upgrade now\" button) and it tries to upgrade Plinth. The Plinth upgrade "
"can fail during this manual upgrade process started through Plinth."
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"There are 2 workarounds for those trying to upgrade from 0.7.x:\n"
"- Turn on Automatic Upgrades, and wait (up to 24 hours). Plinth will be\n"
"  automatically upgraded to 0.8.1, and the issue is avoided.\n"
"- Or, you can SSH into the box, and run \"sudo apt update && sudo apt "
"upgrade\".\n"
msgstr ""

#. type: Plain text
msgid ""
"Thanks to all who helped to put this release together. There were several "
"new contributors for this release. The wiki has a full list of contributors "
"to the project:"
msgstr ""

#. type: Plain text
msgid "<https://wiki.debian.org/FreedomBox/Contributors>"
msgstr ""

#. type: Plain text
msgid ""
"Please feel free to join us to discuss this release on the mailing list, "
"IRC, or on the monthly progress calls:"
msgstr ""

#. type: Plain text
msgid "- List: <http://lists.alioth.debian.org/pipermail/freedombox-discuss/>"
msgstr ""

#. type: Plain text
msgid "- IRC: <irc://irc.debian.org/freedombox>"
msgstr ""

#. type: Plain text
msgid "- Calls: <https://wiki.debian.org/FreedomBox/ProgressCalls>"
msgstr ""

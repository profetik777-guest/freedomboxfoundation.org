# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
#
# Translators:
msgid ""
msgstr ""
"Project-Id-Version: FreedomBox Foundation\n"
"POT-Creation-Date: 2016-02-24 17:43+0000\n"
"PO-Revision-Date: 2011-11-14 19:22+0000\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: Italian (Italy) (http://www.transifex.com/projects/p/"
"freedomboxfoundation/language/it_IT/)\n"
"Language: it_IT\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Vote for FreedomBox in Ashoka Changemaker's Competition\"]]\n"
msgstr ""

#. type: Title #
#, no-wrap
msgid "Vote for FreedomBox in Ashoka Changemaker's Competition"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"<table><tr><td valign=\"top\">\n"
"<img src=\"/images/GOOGLE_badge_vote.png\" /></td><td valign=\"top\">\n"
"</div> <div float=\"right\">The FreedomBox has made it to the final round in Ashoka's Changemakers\n"
"competition and now things will come down to a public vote!  We\n"
"already won the Early Entrant's prize when we first put in our\n"
"application.  And a strong showing in the voting would put the project\n"
"in a strong position for a Fellowship that would fund significant\n"
"project work.\n"
msgstr ""

#. type: Plain text
msgid ""
"Please take a moment to [vote for us](http://www.changemakers.com/"
"citizenmedia?utm_source=contacts-enthus&utm_medium=email-"
"others&utm_content=vote&utm_campaign=citizenmedia)! (Scroll all the way "
"down.)"
msgstr ""

#. type: Plain text
msgid ""
"Signup might be required, though they've assured me they won't spam you.  If "
"you're on Facebook, You can also [vote via your Facebook account](https://"
"apps.facebook.com/changemakers/citizenmedia), which is easier, faster and "
"fraught with privacy implications."
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"And please spread the word.  A few moments of help could mean a lot to\n"
"moving this project forward.\n"
"</td></tr></table>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!sidebar  content=\"\"\"\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"Links:\n"
"[[Home|https://www.freedomboxfoundation.org/]]  \n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[FAQ]]  \n"
msgstr ""

#. type: Plain text
msgid "[[Donate]]"
msgstr ""

[[!meta title="How We're Going To Do This"]]

# How We're Going To Do This

I want to thank all the generous and dedicated contributors who made
our Kickstarter "0 to 60 in 30 days" campaign a resounding success.
More than 1,000 contributors took us from 0 to almost 90 in those 30
days, and we are grateful to each of your for your support.  We will
do our best to justify your confidence.  

Your donations will allow us to begin to coordinate contributions
by volunteers from every corner of the Free World.  Together, we will
work to make our shared vision a reality.

Ours is a large undertaking with many moving parts.  We at the
FreedomBox Foundation are here to help communicate, facilitate, and
spread the FreedomBox project around the world.  We plan to 
administer the effort based on four organizational pillars:

 1. Functional software development and integration;
 2. User experience design, implementation and integration;
 3. Communications and fund-raising; and
 4. Industry relations

Each of these pillars will be led by an advisory committee, with all
activities coordinated by a small full-time staff at the FreedomBox
Foundation.

Advisory Committee membership will evolve, as developers and others 
who commit themselves heavily to the project step up.  Initial 
nominations reflecting early commitments by leaders in our community will 
be announced shortly.  Bdale and I have begun contacting initial members 
of the Technical Advisory Committee that Bdale will chair.  Once
assembled, that Committee's first activity will be to lead the public
development of an initial road-map. 

More announcements concerning process and schedule will appear here
soon.  In addition to our financial contributors, I want to thank also
the wiki editors and mailing list writers who have contributed so many
good ideas and so much positive energy to launch us on this adventure
together.

Eben Moglen

[[!sidebar  content="""

Links:    
[[Home|https://www.freedomboxfoundation.org/]]    
[[FAQ]]    
[[Donate]]    

"""]]

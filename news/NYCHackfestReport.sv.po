# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
#
# Translators:
msgid ""
msgstr ""
"Project-Id-Version: FreedomBox Foundation\n"
"POT-Creation-Date: 2016-02-24 17:43+0000\n"
"PO-Revision-Date: 2012-02-22 15:13+0000\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: Swedish (http://www.transifex.com/projects/p/"
"freedomboxfoundation/language/sv/)\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: Title #
#, no-wrap
msgid "Hackfest Report"
msgstr ""

#. type: Plain text
msgid ""
"The FreedomBox Hackfest at Columbia University was a huge success.  We "
"hosted 25 people of diverse talents and interests.  Some folks came to "
"learn, acquire DreamPlugs and do a guided install of Bdale Garbee's "
"FreedomMaker.  Others took up parts of the (task list)[http://"
"freedomboxfoundations.org].  We learned a lot about the boxes, ideas for "
"routing, data modeling, and security concerns.  Lots of people pitched in on "
"the tasks list, and descriptions of that work are below.  Most importantly, "
"we had a lot of fun meeting each other and collaborating.  If you are "
"interested in any of it, please ask about it on the discussion list, "
"especially if you want to help!"
msgstr ""

#. type: Plain text
msgid ""
"Huge thanks to everybody that participated.  Many people came by just to "
"learn about the FreedomBox, talk about their use cases and offer "
"encouragement.  That activity is very helpful, and we appreciated the fresh "
"perspective."
msgstr ""

#. type: Plain text
msgid ""
"We intend to do more hackfests, perhaps in more cities in the near future.  "
"This weekend was a great success in progress, bug squashing, design, and "
"first-draft implementation of key FreedomBox infrastructure."
msgstr ""

#. type: Plain text
msgid ""
"Thanks again to everybody and especially to Columbia University, Elizabeth "
"Boylan, and ISOC-NY for logistical and material support."
msgstr ""

#. type: Title ##
#, no-wrap
msgid "Progress and Activities"
msgstr ""

#. type: Bullet: ' * '
msgid ""
"Nick Daly, Issac Wilder, Ian Sullivan and I fleshed out my earlier, nascent "
"Port Santiago work (more about that below) and implemented some basic "
"FreedomBox discovery and connection functionality.  Nick and Issac also "
"sketched out a DHT scheme (codenamed Neruda) for finding boxes.  Nick and "
"Issac each did presentations on the routing issue.  Joly Macfie from ISOC-NY "
"took video of those presentations and will get them online this week."
msgstr ""

#. type: Bullet: ' * '
msgid ""
"Nick and Ian iterated hard on paired boxes (a dedicated, hard-coded, factory-"
"set proxy server and client connection) as a way to roll out freedom to "
"small numbers of people stuck behind national firewalls.  We now have paired "
"boxes that locate each other on the network and proxy for each other!"
msgstr ""

#. type: Bullet: ' * '
msgid ""
"Barbara Mack came to us with expertise in data model-ling, and she spent her "
"time doing deep thinking on the person model and how it connects to the rest "
"of the FreedomBox."
msgstr ""

#. type: Bullet: ' * '
msgid ""
"BoxySean installed FreedomBox on a GuruPlug and updated the Wiki with "
"details on how to do that more easily.  There are a lot of GuruPlugs out "
"there, and his work is a big help."
msgstr ""

#. type: Bullet: ' * '
msgid ""
"BoxySean and Brian Newbold started work on configuring the FreedomBox as a "
"wifi router and connecting that up to Plinth for UI.  They identified some "
"FreedomMaker bugs.  BoxySean updated the list on those bugs and we've been "
"following up on closing them."
msgstr ""

#. type: Bullet: ' * '
msgid ""
"A team of people (most notably Marcus and Tyrone) from the [FreedomTower]"
"(http://freenetworkfoundation.org/) project explored ways the FreedomBox can "
"be added to their mobile internet stations to provide Free communication "
"services to people connecting to the net via those stations."
msgstr ""

#. type: Bullet: ' * '
msgid ""
"Sean O'Brien familiarized himself with Plinth and intends to contributing to "
"the router front-end setup effort."
msgstr ""

#. type: Bullet: ' * '
msgid ""
"David Garson dropped knowledge on configuration systems and virtual machines "
"as test beds."
msgstr ""

#. type: Bullet: ' * '
msgid ""
"I updated [Plinth](http://github.com/plinth) with a new template featuring "
"some design work by Robert Martinez (about which I will say more in a "
"separate update)."
msgstr ""

#. type: Bullet: ' * '
msgid ""
"I updated Plinth with a new template and chased down some bugs.  Boxysean "
"explored Plinth's bugs as well, albeit involuntarily.  We'll have patches on "
"those shortly."
msgstr ""

#. type: Title ##
#, no-wrap
msgid "Port Santiago"
msgstr ""

#. type: Plain text
msgid ""
"There is a problem we have been referring to as the \"magic routing problem"
"\".  It is the question of how two FreedomBoxes find each other on the "
"internet and establish communication, even if one or both boxes are "
"firewalled and neither is findable via DNS.  We called it \"magic routing\" "
"because we hadn't started to design the routing system and so we had to "
"assume it happened by magic."
msgstr ""

#. type: Plain text
msgid ""
"Our solution to this problem is to piggyback on the Tor network.  Hidden "
"services rely on Tor for routing and discoverability.  The system works "
"quite well and the Tor project does excellent work at maintaining that "
"system and strengthening it against attack."
msgstr ""

#. type: Plain text
msgid ""
"Nick Daly and Ian Sullivan built a simple server that listens on a local "
"port and is reachable from the outside world by a Tor hidden service.  It "
"accepts authenticated queries and responds with information.  For example, "
"it can give your IP address to friends you trust."
msgstr ""

#. type: Plain text
msgid ""
"To avoid burdening the Tor network and also to avoid the delays associated "
"with using Tor, Port Santiago will allow two FreedomBoxes to decide on a "
"faster (though less anonymous) method of communication.  Subsequent "
"communication will happen on that channel."
msgstr ""

#. type: Plain text
msgid ""
"Right now, we are using Santiago to discover a FreedomBox's IP address for "
"the creation of encrypted proxy tunnels.  This will allow a FreedomBox to "
"provide uncensored, unmonitored internet access to a friend who is stuck "
"behind a national or corporate firewall."
msgstr ""

#. type: Plain text
msgid ""
"Nick did the heavy lifting on this work with help from Ian Sullivan and in "
"consultation with many of the Hackfest participants.  Nick's documentation "
"and code will be up on github soon."
msgstr ""

#. type: Title ##
#, no-wrap
msgid "Neruda"
msgstr ""

#. type: Plain text
msgid ""
"Port Santiago lets FreedomBoxes provide some basic information via a Tor "
"hidden service, but users need a way to find out the onion addresses of "
"their friend's services.  The most obvious way to do this is with "
"distributed hash tables (we also considered less obvious methods that "
"piggyback on existing infrastructure, like bit.ly links).  Issac and I "
"planned out the DHT, how to access it, the data structures, and API.  We are "
"calling this system Neruda."
msgstr ""

#. type: Plain text
msgid ""
"Neruda will allow users to take a GPG key and look up a user's Santiago "
"onion address.  Issac Wilder is speccing this out and coding it."
msgstr ""

#. type: Plain text
msgid ""
"One benefit of reaching Santiago via a Tor onion address is that "
"FreedomBoxes do not need to update Neruda very often.  Even if your IP "
"address changes, your onion address shouldn't.  Updating your Neruda record "
"is a rare event and it is acceptable to refresh Neruda's tables relatively "
"infrequently.  This might allow us to devote fewer resources to it."
msgstr ""

#. type: Title ##
#, no-wrap
msgid "Key Signing"
msgstr ""

#. type: Plain text
msgid ""
"FreedomBoxes are going to make a lot of use of GPG to authenticate identity "
"and sometimes to encrypt data as well.  To strengthen the web of trust, we "
"did some key signing and also introduced some participants to gpg, generated "
"keys and taught key management."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!sidebar  content=\"\"\"\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"Links:\n"
"[[Home|https://www.freedomboxfoundation.org/]]  \n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[FAQ]]  \n"
msgstr ""

#. type: Plain text
msgid "[[Donate]]"
msgstr ""

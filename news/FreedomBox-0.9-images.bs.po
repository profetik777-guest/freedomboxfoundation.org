# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2016-07-15 14:55+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Title ##
#, no-wrap
msgid "FreedomBox 0.9 Images Available"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "*July 15th, 2016*\n"
msgstr ""

#. type: Plain text
msgid ""
"A week ago we have completed building FreedomBox images for 0.9 as 0.9-rc2 "
"and made them available for release testing.  No major issues have been "
"found in the images and they are now the 0.9 final release images."
msgstr ""

#. type: Bullet: '   * '
msgid "<http://ftp.skolelinux.org/pub/freedombox/0.9/>"
msgstr ""

#. type: Bullet: '   * '
msgid "<http://ftp.freedombox.org/pub/freedombox/0.9/>"
msgstr ""

#. type: Title ###
#, no-wrap
msgid "Transition to Debian testing:"
msgstr ""

#. type: Plain text
msgid ""
"With this release, we have completed the transition to Debian \"testing\" "
"from Debian \"unstable\".  From now on, for regular users, images will be "
"based on \"testing\".  When using these images, upgrades will happen to only "
"packages that end up in Debian \"testing\"."
msgstr ""

#. type: Plain text
msgid ""
"If you are already using FreedomBox, you are advised to switch to \"testing"
"\" distribution manually for higher stability.  This can done by replacing "
"\"unstable\" with \"testing\" in /etc/apt/sources.list and waiting for a few "
"weeks for all packages to settle down or by freshly setting up using "
"\"testing\" images."
msgstr ""

#. type: Plain text
msgid ""
"Images based on Debian \"unstable\" will still be built and available as "
"\"nightly\" images so that contributors can test early and prevent the "
"regular users from facing issues.  If you wish to contribute to FreedomBox "
"by finding and reporting early problems, please stick with \"unstable\" "
"images."
msgstr ""

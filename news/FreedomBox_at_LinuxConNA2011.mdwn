[[!meta title="FreedomBox at LinuxConf North America"]]

# FreedomBox at LinuxConf North America

FreedomBox Foundation's founder Eben Moglen and Tech Leader Bdale Garbee will be attending the next Linux Conference North America in Vancouver, Aug 17-19. This year's edition marks the 20th anniversary of Linux kernel, a major milestone for the community.

Bdale Garbee will speak on Wed Aug 17th at 3pm in Plaza B. 
<ul>
<li><a href="http://events.linuxfoundation.org/events/linuxcon/garbee">Freedom, Out of the Box! by Bdale Garbee</a><br />a status update on the development of "FreedomBox", a personal server running a free software operating system and free applications, designed to create and preserve personal privacy by providing a secure platform upon which federated social networks can be constructed.</li>
</ul>

Prof. Eben Moglen will speak at the panel <a href="http://events.linuxfoundation.org/events/linuxcon/20-years-of-linux-panel">20 years of Linux</a> right after Bdale's speech and he will available also during other social events.
Follow us on <a href="http://identi.ca/freedomboxfndn">identi.ca</a>/<a href="http://twitter.com/freedomboxfndn">twitter</a> to get last minute announcements.

[[!sidebar  content="""

Links:
[[Home|https://www.freedomboxfoundation.org/]]  
[[FAQ]]  
[[Donate]]

"""]]

[[!meta title="FreedomBox Update"]]
# FreedomBox Update After DebConf

Many hackers involved in FreedomBox had the chance to meet in Banja Luka
at <a href="http://debconf11.debconf.org/">DebConf11</a>. Bdale Garbee 
gave a speech highlighting the status of the development. The full 
recording of the session is available on <a href="http://penta.debconf.org/dc11_schedule/events/704.en.html">
Debian's site</a>. If you already know the 
basic of FreedomBox project, skip to minute 33 to hear the latest 
development and the next steps.

[[!sidebar  content="""

Links:
[[Home|https://www.freedomboxfoundation.org/]]  
[[FAQ]]  
[[Donate]]

"""]]

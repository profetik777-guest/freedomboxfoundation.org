# FreedomBox Project

The FreedomBox project has a separate website at
[freedombox.org](https://freedombox.org). FreedomBox software is
available for [download](https://www.freedombox.org/download/) for free.
A video introduction to getting started with FreedomBox is available.
FreedomBox project [wiki](https://wiki.debian.org/FreedomBox) has an
extensive [user manual](https://wiki.debian.org/FreedomBox/Manual) and
other helpful material for users. [Release
notes](https://wiki.debian.org/FreedomBox/ReleaseNotes) is good source
of news about the project. Support on using FreedomBox can be obtained
by posting on the [mailing
list](https://lists.alioth.debian.org/mailman/listinfo/freedombox-discuss)
or by asking on the IRC channel [#freedombox on
irc.debian.org](irc://irc.debian.org/freedombox).

The project is actively developed by a horizontal network of
[contributors](https://wiki.debian.org/FreedomBox/Contributors). This
network continues to make tremendous progress in software development,
often without the guidance of the FreedomBox Foundation. The FreedomBox
[project page on
salsa.debian.org](https://salsa.debian.org/freedombox-team/) hosts the
source code, issue tracker and release planner. The wiki has a
[contributor's guide](https://wiki.debian.org/FreedomBox/Contribute) for
coding, designing, documentating, marketing, localizing and QA of
FreedomBox.

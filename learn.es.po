# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
#
# Translators:
# Rodrigo Garcia <absorto@sdf.org>, 2012
# agafex2 <agafex2@gmail.com>, 2012
# Be Helguera <belue@kuririnmail.com>, 2012
#   <clint@debian.org>, 2011
# Funkin, 2012
# j_chan <jennifer.chan.p@gmail.com>, 2012
msgid ""
msgstr ""
"Project-Id-Version: FreedomBox Foundation\n"
"POT-Creation-Date: 2015-02-02 18:17+0000\n"
"PO-Revision-Date: 2014-11-25 21:35+0000\n"
"Last-Translator: fbfwiki <root@softwarefreedom.org>\n"
"Language-Team: Spanish (http://www.transifex.com/projects/p/"
"freedomboxfoundation/language/es/)\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: Title #
#, no-wrap
msgid "Learn About the FreedomBox!"
msgstr "¡Aprende más sobre FreedomBox!"

#. type: Plain text
msgid ""
"Eben Moglen explained the FreedomBox in a short interview with the [CBS "
"Evening News](http://freedomboxfoundation.org/cbs).  We also have a [one "
"page flyer](/doc/flyer.pdf) that explains the box quickly and easily to lay "
"people.  Print and distribute it at your next conference!"
msgstr ""
"Eben Moglen explicó FreedomBox en una breve entrevista con el noticiero [CBS "
"Evening News][http://freedomboxfoundation.org/cbs].\n"
"\n"
"Además tenemos [un panfleto][/doc/flyer.pdf] que explica en pocas palabras "
"el proyecto. ¡Imprímelo y distribúyelo en tu próxima conferencia!"

#. type: Title ##
#, no-wrap
msgid "What is FreedomBox?"
msgstr "¿Qué es FreedomBox?"

#. type: Bullet: '* '
msgid ""
"Email and telecommunications that protects privacy and resists eavesdropping"
msgstr ""
"Correo electrónico y telecomunicaciones que protegen la privacidad y se "
"resisten al espionaje."

#. type: Bullet: '* '
msgid "A publishing platform that resists oppression and censorship."
msgstr "Una plataforma de publicación resistente a la opresión y la censura."

#. type: Bullet: '* '
msgid "An organizing tool for democratic activists in hostile regimes."
msgstr ""
"Una herramienta de organización para activistas por la democracia en "
"regímenes hostiles."

#. type: Bullet: '* '
msgid "An emergency communication network in times of crisis."
msgstr "Una red de comunicación de emergencia para situaciones de crisis."

#. type: Plain text
msgid ""
"FreedomBox will put in people's own hands and under their own control "
"encrypted voice and text communication, anonymous publishing, social "
"networking, media sharing, and (micro)blogging."
msgstr ""
"Freedombox pondrá en las manos de la gente y bajo su propio control "
"comunicación oral y escrita codificada, publicaciones anónimas, redes "
"sociales, intercambio de medios de comunicación y (micro)blogging."

#. type: Plain text
msgid ""
"Much of the software already exists: onion routing, encryption, virtual "
"private networks, etc. There are tiny, low-watt computers known as \"[plug "
"servers](https://secure.wikimedia.org/wikipedia/en/wiki/Plug_computer)\" to "
"run this software. The hard parts is integrating that technology, "
"distributing it, and making it easy to use without expertise. The harder "
"part is to decentralize it so users have no need to rely on and trust "
"centralized infrastructure."
msgstr ""
"La mayor parte del software ya existe: onion routing, codificación, redes "
"virtuales privadas, etc. Existen pequeñas computadoras de bajo voltaje "
"conocidas como “[servidores de enchufe](https://secure.wikimedia.org/"
"wikipedia/en/wiki/Plug_computer)” que pueden ejecutar este software. La "
"parte difícil es integrar esa tecnología, distribuirla y hacerla fácil de "
"usar sin experiencia. Y  la parte más difícil es descentralizarla para que "
"los usuarios no tengan necesidad de apoyarse y confiar en infraestructura "
"centralizada."

#. type: Plain text
msgid ""
"That's what FreedomBox is: we integrate privacy protection on a cheap plug "
"server so everybody can have privacy. Data stays in your home and can't be "
"mined by governments, billionaires, thugs or even gossipy neighbors."
msgstr ""
"Eso es FreedomBox: Integramos la protección de la privacidad en un servidor "
"de enchufe barato para que todos puedan tener privacidad. Los datos se "
"quedan en su hogar y no pueden ser usados para la minería de datos de los "
"gobiernos, billonarios, rufianes o incluso vecinos chismosos."

#. type: Plain text
msgid ""
"With FreedomBoxes in their homes, anybody, regardless of technical skill, "
"can easily enjoy secure, private, even anonymous communication!"
msgstr ""
"Con Freedombox en sus hogares, cualquier persona, sin importar su habilidad "
"técnica, ¡puede disfrutar fácilmente de una comunicación segura, privada e "
"incluso anónima!"

#. type: Title ##
#, no-wrap
msgid "Why FreedomBox?"
msgstr "¿Por qué FreedomBox?"

#. type: Plain text
msgid ""
"FreedomBox integrates privacy protection on a cheap plug server so everybody "
"can have privacy. Data stays in your home and can't be mined by governments, "
"billionaires, thugs or even gossipy neighbors. Other practical examples "
"where FreedomBox is useful:"
msgstr ""
"FreedomBox integra la protección de la privacidad en un servidor de enchufe "
"barato para que todos puedan tener privacidad. Los datos se quedan en su "
"hogar y no pueden ser usados para la minería de datos de los por gobiernos, "
"billonarios, rufianes o incluso vecinos chismosos. Otros ejemplos prácticos "
"en donde se usa FreedomBox son:"

#. type: Bullet: ' * '
msgid ""
"FreedomBoxes are encrypted web proxies. Boxes in uncensored countries can "
"bounce signals for users stuck behind censorship walls---each one is a tiny "
"crack in the Great Firewall. Chinese users could **surf the entire net free "
"from government censorship**."
msgstr ""
"Las cajas de Freedom Box son proxis de la web codificados. Las cajas en los "
"países sin censura pueden rebotar las señales para los usuarios que se "
"encuentran atrapados detrás de las murallas de censura --- cada una es una "
"pequeña grieta en el gran Firewall. Los usuarios chinos podían **navegar "
"toda la red libres de la censura del gobierno**."

#. type: Bullet: ' * '
msgid ""
"The US government famously sought information about internal WikiLeaks "
"communications from Twitter and other social websites. By moving our "
"communication from centralized monoliths to decentralized servers in our "
"homes, we **protect our data from government prying**."
msgstr ""
"El gobierno de los Estados Unidos, como todos saben, buscó información de "
"las comunicaciones internas de WikiLeaks en Twitter y en otros sitios web "
"sociales. Al cambiar nuestra comunicación de monolitos centralizados a "
"servidores descentralizados en nuestros hogares, **protegemos nuestros datos "
"del gobierno entrometido**."

#. type: Bullet: ' * '
msgid ""
"Many whistleblowers and dissidents need to **anonymously talk to media and "
"the public**. With the FreedomBox, we can use VOIP to encrypt telephone "
"calls and can create anonymous web servers over TOR to publish documents. "
"Anonymous instant messaging or microblogging are also possible."
msgstr ""
"Muchos denunciantes y disidentes necesitan ** hablar con los medios de "
"comunicación y con el público de una manera anónima**. Con FreedomBox, "
"podemos usar VOIP para codificar llamadas telefónicas y podemos crear "
"servidores de red anónimos sobre TOR para publicar documentos. También es "
"posible mandar mensajes instantáneos anónimos o microblogging."

#. type: Bullet: ' * '
msgid ""
"Egyptian Democracy activists had trouble talking to demonstrators in the "
"streets because the Mubarak regime shutdown parts of the internet as well as "
"many cellular networks. **If our internet plug is pulled, the box will use "
"mesh routing to talk to other boxes like it**. If any of them can get a "
"packet across the border, they all can."
msgstr ""
"Los activistas de la Democracia Egipcia tuvieron problemas para comunicarse "
"con los manifestantes en las calles porque el régimen de Mubarak  bloqueó "
"partes del internet así como muchas redes para teléfonos celulares. ** Si "
"nuestro cable de internet se desconecta, la caja usará otras rutas en la red "
"inalámbrica para comunicarse con otras cajas iguales**. Si una de ellas "
"puede hacer llegar un paquete de datos al otro lado de la frontera, entonces "
"todas pueden."

#. type: Bullet: ' * '
msgid ""
"FreedomBoxes are useful on a daily personal level too. That same proxy "
"technology can **scrub web sites of ads and tracking** technology as you use "
"them, thus protecting your privacy. FreedomBoxes help you **encrypt your "
"email**. They also know who your friends are and can back up your data in "
"encrypted form to their FreedomBoxes. You can get your data back even if you "
"don't know your password. Even absent a crisis, privacy matters."
msgstr ""
"Las cajas de FreedomBox también son útiles a nivel personal.  Esa misma "
"tecnología de proxis puede ** quitar los anuncios y el rastreo de "
"tecnología** de las páginas de internet cuando las usa, protegiendo de esa "
"manera su privacidad. Las cajas de FreedomBox le ayudan a **codificar su "
"correo electrónico**. También saben quienes son sus amigos y pueden "
"respaldar sus datos de una manera codificada a sus cajas de FreedomBox.  "
"Usted puede recuperar sus datos incluso si no se sabe su contraseña.  Su "
"privacidad es importante aunque no haya una crisis."

#. type: Bullet: ' * '
msgid ""
"FreedomBox is free software, which means you can freely inspect it, audit "
"it, study it and improve it."
msgstr ""
"FreedomBox es software libre, lo que quiere decir que puedes libremente "
"inspeccionarlo, auditarlo, estudiarlo y mejorarlo."

#. type: Plain text
msgid ""
"For a list of specific FreedomBox capabilities, check out our [[Goals]] page."
msgstr ""
"Para ver una lista de capacidades de Freedombox, revise nuestra página de "
"[[Objetivos]]."

#. type: Title ##
#, no-wrap
msgid "Who is FreedomBox?"
msgstr "FreedomBox, quiénes somos."

#. type: Plain text
#, fuzzy
#| msgid ""
#| "FreedomBox is a collaborative project of programmers around the world who "
#| "believe in Free Software, Free Society.  Many of its members will come "
#| "from the [Debian community](http://wiki.debian.org/FreedomBox), and many "
#| "will come from other corners of the Free World.  [Bdale Garbee](http://"
#| "gag.com/~bdale/), former Debian Project Leader is the project's Tech "
#| "Lead.  FreedomBox is organized into [[WorkingGroups]]."
msgid ""
"FreedomBox is a collaborative project of programmers around the world who "
"believe in Free Software, Free Society.  Many of its members will come from "
"the [Debian community](http://wiki.debian.org/FreedomBox), and many will "
"come from other corners of the Free World.  [Bdale Garbee](http://gag.com/"
"~bdale/), former Debian Project Leader is the project's Tech Lead."
msgstr ""
"FreedomBox es un proyecto de colaboración de programadores alrededor del "
"mundo quienes creen en que la Libertad de Software crea una Sociedad Libre. "
"Muchos de sus integrantes vendrán de la [comunidad Debian](http://wiki."
"debian.org/FreedomBox), y muchos vendrán de otras esquinas del Mundo Libre. "
"[Bdale Garbee](http://gag.com/~bdale/), ex-Líder del Proyecto Debian, es el "
"Líder Técnico del proyecto. FreedomBox se organiza en [[Grupos de Trabajo]]."

#. type: Plain text
msgid ""
"The FreedomBox Foundation, which supports the FreedomBox Project and "
"conserves the free software it makes, is led by its President, [Eben Moglen]"
"(http://emoglen.law.columbia.edu/), Professor of Law at Columbia Law School "
"and founder of the [Software Freedom Law Center](http://softwarefreedom."
"org), [Bdale Garbee](http://gag.com/~bdale/), former Debian Project Leader "
"is the project's Tech Lead and President of [Software in the Public Interest]"
"(http://www.spi-inc.org/), and [Yochai Benkler](http://benkler.org/Bio."
"html), Berkman Professor of Entrepreneurial Legal Studies at Harvard, and "
"faculty co-director of the Berkman Center for Internet and Society."
msgstr ""

#. type: Plain text
msgid "A fuller description of our team can be found on the [[team]] page."
msgstr ""
"Puede encontrar una descripción más completa de nuestro equipo en la página "
"del [[equipo]]."

#. type: Title ##
#, no-wrap
msgid "FAQ"
msgstr "FAQ"

#. type: Plain text
msgid ""
"The Foundation published an [[FAQ|faq]] that starts to answer the more "
"common questions about next steps and how we relate to the rest of the "
"community."
msgstr ""
"La Fundación publicó las [[FAQ|faq]] que empiezan a responder las preguntas "
"más comunes acerca de los pasos que siguen y de cómo nos relacionamos con el "
"resto de la comunidad."

#. type: Title ##
#, no-wrap
msgid "Talks, Press and Appearances"
msgstr "Pláticas, Prensa y Presentaciones"

#. type: Plain text
msgid ""
"We announce and document talks, press and appearances [[here|appearances]]."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!sidebar  content=\"\"\"\n"
msgstr "[[!menú lateral | contenido=\"\"\"\n"

#. type: Plain text
#, fuzzy, no-wrap
#| msgid ""
#| "Links:    \n"
#| "[[FAQ]]    \n"
#| "[CBS Evening News](http://freedomboxfoundation.org/cbs)\n"
#| "[Media Archive](http://freedomboxfndn.mirocommunity.org)\n"
msgid ""
"Links:    \n"
"[[FAQ]]    \n"
"[CBS Evening News](http://freedomboxfoundation.org/cbs)\n"
msgstr "Enlaces: |[[FAQ]] | [Noticiero Vespertino de CBS](http://freedomboxfoundation.org/cbs)  [Archivo de Comunicación] (http://freedomboxfndn.mirocommunity.org)\n"

#~ msgid ""
#~ "We announce and document talks, press and appearances [[here|"
#~ "appearances]].  We also collect recordings of FreedomBox Foundation "
#~ "appearances and press on our [Media Archive](http://freedomboxfndn."
#~ "mirocommunity.org)."
#~ msgstr ""
#~ "Anunciamos y documentamos pláticas, prensa y presentaciones [[aquí|"
#~ "presentaciones]]. También recopilamos grabaciones de presentaciones y de "
#~ "prensa de la Fundación Freedombox en nuestro [Archivo de Comunicación] \n"
#~ "(http://freedomboxfndn.mirocommunity.org)."

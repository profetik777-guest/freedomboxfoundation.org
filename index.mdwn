# About the Foundation

## What is a FreedomBox?

[FreedomBox](https://freedombox.org) is a software system that powers an
ultra-secure personal server built to act as a protective shield between
you and the public internet. To that end, it couples a free software
system with always-on, inexpensive, and power-efficient hardware about
the size of a pocket dictionary. The hardware is a single-board computer
that costs about 60 USD and offers the computing power of a smart phone.
The software is a free and open source system available for download at
no cost preloaded with many useful apps and tools designed to protect
your freedom, privacy, and user rights.

Just install our system onto your single-board computer and plug it into
your home network. A web interface acts as the central hub, offering
one-click installs and simple configuration of apps and services. Right
out of the box, you can connect to the internet via your FreedomBox
because it functions as a Wi-Fi access point. In addition, FreedomBox
also comes with numerous apps and services built-in, such as an
anonymity network, encrypted messaging, voice calls, a privacy-defending
VPN, an ad-blocker, an address book, calendering, and a blogging
platform. As a private server, FreedomBox securely hosts all of your
data and protects against data mining, surveillance, and cyberattacks.

## What is the FreedomBox Foundation?

Founded in 2011 by Eben Moglen, the FreedomBox Foundation is a
non-profit organization with 501(c)(3) status that exists to coordinate,
support and advocate for the FreedomBox community. Because FreedomBox is
developed by a collaborative community of programmers across the globe,
the foundation serves as a central hub that unifies the FreedomBox
community. Based in New York City, the FreedomBox Foundation conducts
the project's legal work, coordinates the activities of software
developers, investors, and contributors, and strategizes the future of
FreedomBox. The foundation also provides technical and logistical
support to a growing team of software developers. Lastly, the foundation
serves as the public face of FreedomBox by conducting outreach to the
user community, organizing events and conferences, and working with
partner organizations.

The FreedomBox Foundation's goal is to become the mainstream solution to
privacy and security concerns for activists, journalists, businesses,
and even ordinary people worldwide. From villages in rural India to
dorms at American universities, the community of current FreedomBox
users is large, but we want to make it even larger. People increasingly
report feeling hopeless against mass surveillance, cyberattacks, and
data-mining; it is this hopelessness that motivates our team to create a
better future for the net. By advocating for the FreedomBox community,
the foundation aims to restore hope in a free and open internet and
reaffirm the primacy of user rights, one user at a time.

As a non-profit organization with 501(c)(3) status, the FreedomBox
Foundation does not seek to profit off of the FreedomBox project. In
fact, the FreedomBox system is free for download. Our activities are
charitable and educational because we want to make our software
universally available to people across the globe, regardless of
nationality, wealth, and status.

The FreedomBox Foundation is led by its President, [Eben
Moglen](http://emoglen.law.columbia.edu/) (Professor of Law at Columbia
Law School and founder of the [Software Freedom Law
Center](http://softwarefreedom.org/)), Technical Lead and Member of the
Board [Bdale Garbee](http://gag.com/%7Ebdale/) (former Debian Project
Leader and President of [Software in the Public
Interest](http://www.spi-inc.org/)), and Member of the Board [Yochai
Benkler](http://benkler.org/Bio.html) (Berkman Professor of
Entrepreneurial Legal Studies at Harvard and faculty co-director of the
Berkman Center for Internet and Society).

